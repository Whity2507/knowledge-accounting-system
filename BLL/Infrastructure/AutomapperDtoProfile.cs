﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;
using System.Linq;

namespace BLL.Infrastructure
{
    public class AutomapperDtoProfile : Profile
    {
        public AutomapperDtoProfile()
        {
            CreateMap<Article, ArticleDto>()
                .ForMember(y => y.CategoryId, x => x.MapFrom(article => article.CategoryId))
                .ReverseMap();

            CreateMap<Category, CategoryDto>()
                .ReverseMap();

            CreateMap<ArticleTag, ArticleTagDto>()
                .ReverseMap();

            CreateMap<Comment, CommentDto>()
                .ForMember(y => y.UserId, x => x.MapFrom(comment => comment.UserProfileId))
                .ForMember(y => y.NickName, x => x.MapFrom(comment => comment.UserProfile.NickName))
                .ForMember(y => y.ArticleId, x => x.MapFrom(comment => comment.ArticleId))
                .ForMember(y => y.ArticleName, x => x.MapFrom(comment => comment.Article.Name));

            CreateMap<CommentDto, Comment>()
                .ForMember(y => y.UserProfileId, x => x.MapFrom(commentDto => commentDto.UserId));

            CreateMap<TechnologyTag, TechnologyTagDto>()
                .ReverseMap();

            CreateMap<Report, ReportDto>()
                .ReverseMap();

            CreateMap<ApplicationUser, UserDto>()
                .ForMember(y => y.NickName, x => x.MapFrom(userProfile => userProfile.UserProfile.NickName))
                .ForMember(y => y.EnglishLevel, x => x.MapFrom(userProfile => userProfile.UserProfile.EnglishLevel))
                .ForMember(y => y.UserTags, x => x.MapFrom(userProfile => userProfile.UserProfile.TechnologyTags.Select(z => z.Name)))
                .ReverseMap();

            CreateMap<UserProfile, UserDto>()
                .ReverseMap();
        }
    }
}
