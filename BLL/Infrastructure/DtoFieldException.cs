﻿using System;

namespace BLL.Infrastructure
{
    public class DtoFieldException : Exception
    {
        public DtoFieldException() : base() { }
        public DtoFieldException(string message) : base(message) { }
    }
}
