﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICategoryService : ICrud<CategoryDto>
    {
        /// <summary>
        /// Sets IsDeleted flag of category to false.
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        /// <exception cref="KeyNotFoundException">Throw if category with such <see cref="id"/> does not exist</exception>
        /// <exception cref="InvalidOperationException">Throw if category IsDeleted flag equals false</exception>
        Task RestoreByIdAsync(int id);

        /// <summary>
        /// Returns all categories where IsDeleted flag of category equals true.
        /// </summary>
        /// <returns><see cref="IEnumerable{}"/> of <see cref="CategoryDto"/> objects.</returns>
        IEnumerable<CategoryDto> GetAllWithDeleted();
    }
}
