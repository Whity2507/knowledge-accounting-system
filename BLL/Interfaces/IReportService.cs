﻿using BLL.DTO;

namespace BLL.Interfaces
{
    public interface IReportService : ICrud<ReportDto>
    {
    }
}
