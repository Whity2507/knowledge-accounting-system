﻿using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IArticleService : ICrud<ArticleDto>
    {
        /// <summary>
        /// Return random article from database
        /// </summary>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="ArticleDto"/></returns>
        Task<ArticleDto> GetRandomArticleAsync();
    }
}
