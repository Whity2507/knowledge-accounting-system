﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Infrastructure;

namespace BLL.Interfaces
{
    /// <summary>
    /// Describes all CRUD methods.
    /// </summary>
    /// <typeparam name="T">DTO object.</typeparam>
    public interface ICrud<T>
    {
        /// <summary>
        /// Maps model to entity and adds it to database.
        /// </summary>
        /// <param name="model">Data transfer object of <see cref="(T)"/></param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        /// <exception cref="DtoFieldException">Throw if one of the required fields of <see cref="model"/> is null or empty</exception>
        Task AddAsync(T model);

        /// <summary>
        /// Maps model to entity and updates it in database.
        /// </summary>
        /// <param name="model">Data transfer object of <see cref="(T)"/></param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        /// <exception cref="DtoFieldException">Throw if one of the required fields of <see cref="model"/> is null or empty</exception>
        Task UpdateAsync(T model);

        /// <summary>
        /// Delete entity with certain id from database.
        /// </summary>
        /// <param name="id">Id of entity in database</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task DeleteByIdAsync(int id);

        /// <summary>
        /// Maps entity from database with certain id to <see cref="T"/> and returns it.
        /// </summary>
        /// <param name="id">Id of entity in database</param>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="T"/>.</returns>
        /// <exception cref="KeyNotFoundException">Throw if entity with such <see cref="id"/> does not exist</exception>
        Task<T> GetByIdAsync(int id);

        /// <summary>
        /// Maps entities from database to type <see cref="{T}"/> and returns a collection of them.
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of DTO objects.</returns>
        IEnumerable<T> GetAll();
    }
}
