﻿using BLL.DTO;

namespace BLL.Interfaces
{
    public interface ITechnologyTagService : ICrud<TechnologyTagDto>
    {
    }
}
