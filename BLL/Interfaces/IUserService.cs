﻿using BLL.DTO;
using DAL.Entities;
using BLL.Infrastructure;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Adds a new user to the database and sets his role to "Reader".
        /// </summary>
        /// <param name="userDto"><see cref="UserDto"/> object</param>
        /// <returns>A task that represents the asynchronous operation that can return <see cref="IdentityResult"/> object that contains the result of adding new user.</returns>
        /// <exception cref="DtoFieldException">Throw if one of the required fields (Email, Password, Nickname) is null or empty</exception>
        Task<IdentityResult> AddAsync(UserDto userDto);

        /// <summary>
        /// Authenticate user in system.
        /// </summary>
        /// <param name="userDto"><see cref="UserDto"/> object</param>
        /// <returns>A task that represents the asynchronous operation that can return <see cref="ClaimsIdentity"/> object that contains the result of this user authentification.</returns>
        /// <exception cref="DtoFieldException">Throw if one of the required fields (Email, Password) is null or empty</exception>
        /// <exception cref="InvalidOperationException">Throw if the required set is from Email and Password not found or if account is locked</exception>
        Task<ClaimsIdentity> AuthenticateAsync(UserDto userDto);

        /// <summary>
        /// Updates public profile of user.
        /// </summary>
        /// <param name="userDto"><see cref="UserDto"/> object</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        /// <exception cref="DtoFieldException">Throw if one of the required fields (Id, NickName) is null or empty</exception>
        Task UpdateProfileAsync(UserDto userDto);

        /// <summary>
        /// Updates set of roles for certain user by its id.
        /// </summary>
        /// <param name="id">User id</param>
        /// <param name="editedRoles"><see cref="List{string}"/> of role names</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        /// <exception cref="DtoFieldException">Throw if one of the required fields (Id, NickName) is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Throw if user with such <see cref="id"/> does not exist</exception>
        Task UpdateUserRolesAsync(string id, List<string> editedRoles);

        /// <summary>
        /// Sets LockoutEndDateUtc of certain user to DateTime.MaxValue.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        /// <exception cref="KeyNotFoundException">Throw if user with such <see cref="id"/> does not exist</exception>
        Task LockUserByIdAsync(string id);

        /// <summary>
        /// Sets LockoutEndDateUtc of certain user to null.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        /// <exception cref="KeyNotFoundException">Throw if user with such <see cref="id"/> does not exist</exception>
        Task UnlockUserByIdAsync(string id);

        /// <summary>
        /// Returns <see cref="UserDto"/> with all fields from <see cref="UserProfile"/>, <see cref="List{string}"/> of all technology tag names as AllTags field and <see cref="List{string}"/> of all technology tag names of this user as UserTags field.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="UserDto"/></returns>
        /// <exception cref="KeyNotFoundException">Throw if user with such <see cref="id"/> does not exist</exception>
        Task<UserDto> GetUserProfileByIdAsync(string id);

        /// <summary>
        /// Returns <see cref="UserDto"/> with only Id and Email fileds.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="UserDto"/></returns>
        /// <exception cref="KeyNotFoundException">Throw if user with such <see cref="id"/> does not exist</exception>
        Task<UserDto> GetUserEmailByIdAsync(string id);

        /// <summary>
        /// Returns <see cref="UserDto"/> with Id, Email, UserRoles as <see cref="List{string}"/> and AllRoles as <see cref="List{string}"/>.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="UserDto"/></returns>
        /// <exception cref="KeyNotFoundException">Throw if user with such <see cref="id"/> does not exist</exception>
        Task<UserDto> GetUserWithRolesByIdAsync(string id);

        /// <summary>
        /// Returns all users, except administrators and managers.
        /// </summary>
        /// <returns><see cref="IEnumerable{}"/> of <see cref="UserDto"/> objects from database.</returns>
        IEnumerable<UserDto> GetUserProfilesWithEmail();

        /// <summary>
        /// Returns all users.
        /// </summary>
        /// <returns><see cref="IEnumerable{}"/> of <see cref="UserDto"/> objects from database.</returns>
        IEnumerable<UserDto> GetAll();
    }
}
