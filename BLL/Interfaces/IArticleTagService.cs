﻿using BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IArticleTagService : ICrud<ArticleTagDto>
    {
        /// <summary>
        /// Return tags that related to article with certain id
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="List{ArticleTagDto}"/></returns>
        /// <exception cref="KeyNotFoundException">Throw if article with such <see cref="id"/> does not exist</exception>
        Task<List<ArticleTagDto>> GetTagsByArticleIdAsync(int id);
    }
}
