﻿using BLL.DTO;

namespace BLL.Interfaces
{
    public interface ICommentService : ICrud<CommentDto>
    {
    }
}
