﻿using BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IFilterService
    {
        /// <summary>
        /// Returns list of all technology tag names.
        /// </summary>
        /// <returns><see cref="List{string}"/> of tag names from database.</returns>
        List<string> GetAllTechnologyTagNames();

        /// <summary>
        /// Returns articles of certain category by category id.
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns><see cref="IEnumerable{}"/> of <see cref="ArticleDto"/> objects.</returns>
        /// <exception cref="KeyNotFoundException">Throw if category with such <see cref="id"/> does not exist</exception>
        Task<IEnumerable<ArticleDto>> GetArticlesWithDetailsByCategoryIdAsync(int id);

        /// <summary>
        /// Returns articles with certain tag by tag id.
        /// </summary>
        /// <param name="id">Article tag id</param>
        /// <returns><see cref="IEnumerable{}"/> of <see cref="ArticleDto"/> objects.</returns>
        /// <exception cref="KeyNotFoundException">Throw if tag with such <see cref="id"/> does not exist</exception>
        Task<IEnumerable<ArticleDto>> GetArticlesWithDetailsByTagIdAsync(int id);

        /// <summary>
        /// Return all users, except administrators and managers, who have one of the selected levels of English and who have any of selected tags.
        /// </summary>
        /// <param name="englishLevels"><see cref="List{string}"/> of English levels names</param>
        /// <param name="technoloyTags"><see cref="List{string}"/> of technology tags names</param>
        /// <returns><see cref="IEnumerable{}"/> of <see cref="UserDto"/> objects.</returns>
        IEnumerable<UserDto> GetUsersByFilter(List<string> englishLevels, List<string> technoloyTags);
    }
}
