﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class FilterService : IFilterService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public FilterService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<string> GetAllTechnologyTagNames()
        {
            return _unitOfWork.TechnologyTags.GetAll().Select(x => x.Name).ToList();
        }

        public async Task<IEnumerable<ArticleDto>> GetArticlesWithDetailsByCategoryIdAsync(int id)
        {
            var category = await _unitOfWork.Categories.GetByIdAsync(id);
            if (category == null)
            {
                throw new KeyNotFoundException("There is no such category");
            }
            var articles = _unitOfWork.Articles.GetAllWithDetails();
            var articleDtos = _mapper.Map<IEnumerable<ArticleDto>>(articles.Where(x => x.CategoryId == id)).OrderByDescending(x => x.LastChangeDate);
            return articleDtos;
        }

        public async Task<IEnumerable<ArticleDto>> GetArticlesWithDetailsByTagIdAsync(int id)
        {
            var tag = await _unitOfWork.ArticleTags.GetByIdAsync(id);
            if(tag == null)
            {
                throw new KeyNotFoundException("There is no such tag");
            }
            var allArticles = await _unitOfWork.Articles.GetAllWithDetails().ToListAsync();
            allArticles.RemoveAll(x => !x.ArticleTags.Any(y => y.Id == id));

            return _mapper.Map<IEnumerable<ArticleDto>>(allArticles).OrderByDescending(x => x.LastChangeDate);
        }

        public IEnumerable<UserDto> GetUsersByFilter(List<string> englishLevels, List<string> technoloyTags)
        {
            ApplicationRole administratorRole = _unitOfWork.RoleManager.Roles.FirstOrDefault(r => r.Name == "Administrator");
            ApplicationRole managerRole = _unitOfWork.RoleManager.Roles.FirstOrDefault(r => r.Name == "Manager");

            List<UserDto> userDtos = _mapper.Map<List<UserDto>>(_unitOfWork.UserManager.Users
                .Include(x => x.UserProfile)
                .Include(x => x.UserProfile.TechnologyTags)
                .Where(x => x.Roles.Any(y => y.RoleId != administratorRole.Id && y.RoleId != managerRole.Id)).ToList());

            if (englishLevels.Any())
            {
                userDtos.RemoveAll(user => !englishLevels.Any(y => y == user.EnglishLevel));
            }

            if (technoloyTags != null)
            {
                userDtos.RemoveAll(user => !user.UserTags.Any(x => technoloyTags.Any(y => y == x)));
            }

            return userDtos;
        }
    }
}
