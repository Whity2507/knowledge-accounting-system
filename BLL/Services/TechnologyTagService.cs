﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TechnologyTagService : ITechnologyTagService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public TechnologyTagService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(TechnologyTagDto technologyTagDto)
        {
            if (string.IsNullOrEmpty(technologyTagDto.Name))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.TechnologyTags.Add(_mapper.Map<TechnologyTag>(technologyTagDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(TechnologyTagDto technologyTagDto)
        {
            if (string.IsNullOrEmpty(technologyTagDto.Name))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.TechnologyTags.Update(_mapper.Map<TechnologyTag>(technologyTagDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.TechnologyTags.DeleteByIdAsync(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<TechnologyTagDto> GetByIdAsync(int id)
        {
            var technologyTag = await _unitOfWork.TechnologyTags.GetByIdAsync(id);
            if (technologyTag == null)
            {
                throw new KeyNotFoundException("There is no such technology tag");
            }
            return _mapper.Map<TechnologyTagDto>(technologyTag);
        }

        public IEnumerable<TechnologyTagDto> GetAll()
        {
            return _mapper.Map<IEnumerable<TechnologyTagDto>>(_unitOfWork.TechnologyTags.GetAll());
        }
    }
}
