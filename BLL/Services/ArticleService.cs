﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BLL.Services
{
    ///<inheritdoc/>
    public class ArticleService : IArticleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public ArticleService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(ArticleDto articleDto)
        {
            if (string.IsNullOrEmpty(articleDto.Name) || string.IsNullOrEmpty(articleDto.Text) || articleDto.EditedTagIds == null)
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }

            articleDto.Text = SanitizeHtml(articleDto.Text);
            articleDto.PostDate = DateTime.Now;
            articleDto.LastChangeDate = DateTime.Now;
            Article article = _mapper.Map<Article>(articleDto);

            Category category = await _unitOfWork.Categories.GetByIdAsync(articleDto.CategoryId);
            if (category == null)
            {
                throw new KeyNotFoundException("Category not found");
            }
            article.Category = category;
            article.CategoryId = category.Id;

            foreach (var tagId in articleDto.EditedTagIds)
            {
                ArticleTag articleTag = await _unitOfWork.ArticleTags.GetByIdAsync(tagId);
                article.ArticleTags.Add(articleTag);
            }

            _unitOfWork.Articles.Add(article);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(ArticleDto articleDto)
        {
            if (string.IsNullOrEmpty(articleDto.Name) || string.IsNullOrEmpty(articleDto.Text) || articleDto.EditedTagIds == null)
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            articleDto.Text = SanitizeHtml(articleDto.Text);
            articleDto.LastChangeDate = DateTime.Now;
            Article article = _mapper.Map<Article>(articleDto);
            
            _unitOfWork.Articles.Update(article);
            await UpdateArticleTagsAsync(article.Id, articleDto.EditedTagIds);
            await _unitOfWork.SaveAsync();
        }

        private async Task UpdateArticleTagsAsync(int id, List<int> editedTagIds)
        {
            Article article = await _unitOfWork.Articles.GetByIdWithDetailsAsync(id);
            if (article == null)
            {
                throw new KeyNotFoundException("There is no such article");
            }
            List<int> articleTagIds = article.ArticleTags.Select(x => x.Id).ToList();

            if (editedTagIds != null)
            {
                IEnumerable<int> addedTagIds = editedTagIds.Except(articleTagIds);
                foreach (var tagId in addedTagIds)
                {
                    ArticleTag articeTag = await _unitOfWork.ArticleTags.GetByIdWithDetailsAsync(tagId);
                    article.ArticleTags.Add(articeTag);
                    articeTag.Articles.Add(article);

                    _unitOfWork.ArticleTags.Update(articeTag);
                }
            }

            if (editedTagIds == null)
            {
                editedTagIds = new List<int>();
            }

            IEnumerable<int> removedTagIds = articleTagIds.Except(editedTagIds);
            foreach (var tagId in removedTagIds)
            {
                ArticleTag articeTag = article.ArticleTags.FirstOrDefault(x => x.Id == tagId);
                article.ArticleTags.Remove(articeTag);
                articeTag.Articles.Remove(article);

                _unitOfWork.ArticleTags.Update(articeTag);
            }

            _unitOfWork.Articles.Update(article);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.Articles.DeleteByIdAsync(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<ArticleDto> GetByIdAsync(int id)
        {
            Article article = await _unitOfWork.Articles.GetByIdWithDetailsAsync(id);
            if (article == null)
            {
                throw new KeyNotFoundException("There is no such article");
            }
            return _mapper.Map<ArticleDto>(article);
        }

        /// <summary>
        /// Returns articles ordered by last changing date descending.
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of DTO objects.</returns>
        public IEnumerable<ArticleDto> GetAll()
        {
            return _mapper.Map<IEnumerable<ArticleDto>>(_unitOfWork.Articles.GetAllWithDetails().OrderByDescending(x => x.LastChangeDate));
        }

        public async Task<ArticleDto> GetRandomArticleAsync()
        {
            Random rng = new Random();
            var articleId = rng.Next(1, _unitOfWork.Articles.GetAll().Count());
            return _mapper.Map<ArticleDto>(await _unitOfWork.Articles.GetByIdWithDetailsAsync(articleId));
        }

        private string SanitizeHtml(string text)
        {
            string acceptable = "b|i|code";
            string stringPattern = @"</?(?(?=" + acceptable + @")notag|[a-zA-Z0-9]+)(?:\s[a-zA-Z0-9\-]+=?(?:(["",']?).*?\1?)?)*\s*/?>";
            return Regex.Replace(text, stringPattern, " ");
        }
    }
}
