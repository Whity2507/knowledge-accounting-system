﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ReportService : IReportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public ReportService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(ReportDto reportDto)
        {
            if (string.IsNullOrEmpty(reportDto.Text) || string.IsNullOrEmpty(reportDto.UserEmail))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.Reports.Add(_mapper.Map<Report>(reportDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(ReportDto reportDto)
        {
            if (string.IsNullOrEmpty(reportDto.Text) || string.IsNullOrEmpty(reportDto.UserEmail))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.Reports.Update(_mapper.Map<Report>(reportDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.Reports.DeleteByIdAsync(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<ReportDto> GetByIdAsync(int id)
        {
            var report = await _unitOfWork.Reports.GetByIdAsync(id);
            if (report == null)
            {
                throw new KeyNotFoundException("There is no such record in report list");
            }
            return _mapper.Map<ReportDto>(await _unitOfWork.Reports.GetByIdAsync(id));
        }

        public IEnumerable<ReportDto> GetAll()
        {
            return _mapper.Map<IEnumerable<ReportDto>>(_unitOfWork.Reports.GetAll());
        }
    }
}
