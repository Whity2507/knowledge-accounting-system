﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public CommentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CommentDto commentDto)
        {
            Article article = await _unitOfWork.Articles.GetByIdAsync(commentDto.ArticleId);
            if (article == null)
            {
                throw new KeyNotFoundException("There is no such article");
            }
            UserProfile userProfile = await _unitOfWork.UserProfiles.GetByIdAsync(commentDto.UserId);
            if (userProfile == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }

            if (string.IsNullOrEmpty(commentDto.Text))
            {
                throw new DtoFieldException("Comment text must not be empty");
            }

            var comment = new Comment
            {
                Text = commentDto.Text,
                ArticleId = commentDto.ArticleId,
                UserProfileId = commentDto.UserId,
                Article = article,
                UserProfile = userProfile,
                PostDate = DateTime.Now
            };

            _unitOfWork.Comments.Add(comment);
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(CommentDto commentDto)
        {
            if (string.IsNullOrEmpty(commentDto.Text))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            Comment comment = _mapper.Map<Comment>(commentDto);
            comment.Article = await _unitOfWork.Articles.GetByIdAsync(commentDto.ArticleId);
            comment.UserProfile = await _unitOfWork.UserProfiles.GetByIdAsync(commentDto.UserId);

            _unitOfWork.Comments.Update(comment);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.Comments.DeleteByIdAsync(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<CommentDto> GetByIdAsync(int id)
        {
            Comment commentDto = await _unitOfWork.Comments.GetByIdWithDetailsAsync(id);
            if (commentDto == null)
            {
                throw new KeyNotFoundException("There is no such comment");
            }
            return _mapper.Map<CommentDto>(commentDto);
        }

        public IEnumerable<CommentDto> GetAll()
        {
            return _mapper.Map<IEnumerable<CommentDto>>(_unitOfWork.Comments.GetAll());
        }
    }
}
