﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ArticleTagService : IArticleTagService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public ArticleTagService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<List<ArticleTagDto>> GetTagsByArticleIdAsync(int id)
        {
            Article article = await _unitOfWork.Articles.GetByIdWithDetailsAsync(id);
            if (article == null)
            {
                throw new KeyNotFoundException("There is no such article");
            }
            return _mapper.Map<List<ArticleTagDto>>(article.ArticleTags);
        }

        public async Task AddAsync(ArticleTagDto articleTagDto)
        {
            if (string.IsNullOrEmpty(articleTagDto.Name))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.ArticleTags.Add(_mapper.Map<ArticleTag>(articleTagDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(ArticleTagDto articleTagDto)
        {
            if (string.IsNullOrEmpty(articleTagDto.Name))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.ArticleTags.Update(_mapper.Map<ArticleTag>(articleTagDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.ArticleTags.DeleteByIdAsync(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task<ArticleTagDto> GetByIdAsync(int id)
        {
            var articleTag = await _unitOfWork.ArticleTags.GetByIdAsync(id);
            if (articleTag == null)
            {
                throw new KeyNotFoundException("There is no such article tag");
            }
            return _mapper.Map<ArticleTagDto>(articleTag);
        }

        public IEnumerable<ArticleTagDto> GetAll()
        {
            return _mapper.Map<IEnumerable<ArticleTagDto>>(_unitOfWork.ArticleTags.GetAll());
        }
    }
}
