﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IdentityResult> AddAsync(UserDto userDto)
        {
            if (string.IsNullOrEmpty(userDto.Email) || string.IsNullOrEmpty(userDto.Password) || string.IsNullOrEmpty(userDto.NickName))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }

            ApplicationUser applicationUser = new ApplicationUser { UserName = userDto.Email, Email = userDto.Email };
            IdentityResult identityResult = await _unitOfWork.UserManager.CreateAsync(applicationUser, userDto.Password);

            if (identityResult.Succeeded)
            {
                await _unitOfWork.UserManager.AddToRoleAsync(applicationUser.Id, "Reader");
                UserProfile userProfile = new UserProfile { Id = applicationUser.Id, NickName = userDto.NickName };
                _unitOfWork.UserProfiles.Add(userProfile);
                await _unitOfWork.SaveAsync();
            }
            else
            {
                throw new InvalidOperationException("This Email is already taken");
            }

            return identityResult;
        }

        public async Task<ClaimsIdentity> AuthenticateAsync(UserDto userDto)
        {
            if (string.IsNullOrEmpty(userDto.Email) || string.IsNullOrEmpty(userDto.Password))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }

            ApplicationUser applicationUser = await _unitOfWork.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (applicationUser == null)
            {
                throw new InvalidOperationException("Wrong email or password");
            }

            if (_unitOfWork.UserManager.IsLockedOut(applicationUser.Id))
            {
                throw new InvalidOperationException("This account is locked");
            }

            ClaimsIdentity claim = await _unitOfWork.UserManager.CreateIdentityAsync(applicationUser, DefaultAuthenticationTypes.ApplicationCookie);

            return claim;
        }

        public async Task UpdateProfileAsync(UserDto userDto)
        {
            if (string.IsNullOrEmpty(userDto.Id) || string.IsNullOrEmpty(userDto.NickName))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }

            UserProfile userProfile = _mapper.Map<UserProfile>(userDto);

            _unitOfWork.UserProfiles.Update(userProfile);
            await UpdateUserTagsAsync(userDto.Id, userDto.EditedTags);
            await _unitOfWork.SaveAsync();
        }

        private async Task UpdateUserTagsAsync(string id, List<string> editedTagNames)
        {
            UserProfile userProfile = await _unitOfWork.UserProfiles.GetByIdWithDetailsAsync(id);
            if (userProfile == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }
            List<string> userTagNames = userProfile.TechnologyTags.Select(x => x.Name).ToList();

            if (editedTagNames != null)
            {
                var addedTagNames = editedTagNames.Except(userTagNames);
                foreach (var tag in addedTagNames)
                {
                    TechnologyTag technologyTag = await _unitOfWork.TechnologyTags.GetAllWithDetails().FirstOrDefaultAsync(x => x.Name == tag);
                    userProfile.TechnologyTags.Add(technologyTag);
                    technologyTag.UserProfiles.Add(userProfile);

                    _unitOfWork.TechnologyTags.Update(technologyTag);
                }
            }

            if (editedTagNames == null)
            {
                editedTagNames = new List<string>();
            }

            var removedTagNames = userTagNames.Except(editedTagNames);
            foreach (var tag in removedTagNames)
            {
                TechnologyTag technologyTag = userProfile.TechnologyTags.FirstOrDefault(x => x.Name == tag);
                userProfile.TechnologyTags.Remove(technologyTag);
                technologyTag.UserProfiles.Remove(userProfile);

                _unitOfWork.TechnologyTags.Update(technologyTag);
            }

            _unitOfWork.UserProfiles.Update(userProfile);
        }

        public async Task UpdateUserRolesAsync(string id, List<string> editedRoles)
        {
            ApplicationUser applicationUser = await _unitOfWork.UserManager.FindByIdAsync(id);
            if (applicationUser == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }

            IList<string> userRoles = await _unitOfWork.UserManager.GetRolesAsync(id);

            string[] addedRoles = editedRoles.Except(userRoles).ToArray();
            string[] removedRoles = userRoles.Except(editedRoles).ToArray();

            await _unitOfWork.UserManager.AddToRolesAsync(id, addedRoles);
            await _unitOfWork.UserManager.RemoveFromRolesAsync(id, removedRoles);
        }

        public async Task LockUserByIdAsync(string id)
        {
            ApplicationUser applicationUser = await _unitOfWork.UserManager.FindByIdAsync(id);
            if(applicationUser == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }
            applicationUser.LockoutEnabled = true;
            applicationUser.LockoutEndDateUtc = DateTime.MaxValue;
            await _unitOfWork.SaveAsync();
        }

        public async Task UnlockUserByIdAsync(string id)
        {
            ApplicationUser applicationUser = await _unitOfWork.UserManager.FindByIdAsync(id);
            if (applicationUser == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }

            applicationUser.LockoutEndDateUtc = null;
            await _unitOfWork.SaveAsync();
        }

        public async Task<UserDto> GetUserProfileByIdAsync(string id)
        {
            UserProfile userProfile = await _unitOfWork.UserProfiles.GetByIdWithDetailsAsync(id);
            if (userProfile == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }
            List<string> userTags = userProfile.TechnologyTags.Select(x => x.Name).ToList();
            List<string> allTags = _unitOfWork.TechnologyTags.GetAll().Select(x => x.Name).ToList();

            var userDto = _mapper.Map<UserDto>(userProfile);
            userDto.UserTags = userTags;
            userDto.AllTags = allTags;

            return userDto;
        }

        public async Task<UserDto> GetUserEmailByIdAsync(string id)
        {
            ApplicationUser applicationUser = await _unitOfWork.UserManager.FindByIdAsync(id);
            if (applicationUser == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }

            UserDto userDto = new UserDto
            {
                Id = applicationUser.Id,
                Email = applicationUser.Email
            };

            return userDto;
        }

        public async Task<UserDto> GetUserWithRolesByIdAsync(string id)
        {
            ApplicationUser applicationUser = await _unitOfWork.UserManager.FindByIdAsync(id);
            if (applicationUser == null)
            {
                throw new KeyNotFoundException("There is no such user");
            }

            var userRoles = await _unitOfWork.UserManager.GetRolesAsync(id);
            var allRoles = _unitOfWork.RoleManager.Roles.Select(x => x.Name).ToList();
            
            var userDto = new UserDto
            {
                Id = applicationUser.Id,
                Email = applicationUser.Email,
                UserRoles = userRoles as List<string>,
                AllRoles = allRoles
            };

            return userDto;
        }

        public IEnumerable<UserDto> GetUserProfilesWithEmail()
        {
            ApplicationRole administratorRole = _unitOfWork.RoleManager.Roles.FirstOrDefault(r => r.Name == "Administrator");
            ApplicationRole managerRole = _unitOfWork.RoleManager.Roles.FirstOrDefault(r => r.Name == "Manager");

            var users = _unitOfWork.UserManager.Users.Include(x => x.UserProfile).Where(x => x.Roles.Any(y => y.RoleId != administratorRole.Id && y.RoleId != managerRole.Id));
            return _mapper.Map<IEnumerable<UserDto>>(users);
        }

        public IEnumerable<UserDto> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDto>>(_unitOfWork.UserManager.Users.Include(x => x.UserProfile));
        }
    }
}