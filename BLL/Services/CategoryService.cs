﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUnitOfWork and IMapper implementation
        /// </summary>
        /// <param name="unitOfWork">Reference to the IUnitOfWork interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CategoryDto categoryDto)
        {
            if (string.IsNullOrEmpty(categoryDto.Name) || string.IsNullOrEmpty(categoryDto.Description))
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.Categories.Add(_mapper.Map<Category>(categoryDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task UpdateAsync(CategoryDto categoryDto)
        {
            if (string.IsNullOrEmpty(categoryDto.Name) || string.IsNullOrEmpty(categoryDto.Description) )
            {
                throw new DtoFieldException("Model contains null or empty reqired fields");
            }
            _unitOfWork.Categories.Update(_mapper.Map<Category>(categoryDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            Category category = await _unitOfWork.Categories.GetByIdAsync(id);
            if (category == null)
            {
                throw new KeyNotFoundException("There is no such category");
            }
            if (category.IsDeleted)
            {
                throw new InvalidOperationException("This category is already deleted");
            }

            await _unitOfWork.Categories.DeleteByIdAsync(id);
            await _unitOfWork.SaveAsync();
        }

        public async Task RestoreByIdAsync(int id)
        {
            Category category = await _unitOfWork.Categories.GetByIdAsync(id);
            if (category == null)
            {
                throw new KeyNotFoundException("There is no such category");
            }
            if (!category.IsDeleted)
            {
                throw new InvalidOperationException("This category is not deleted");
            }

            category.IsDeleted = false;
            _unitOfWork.Categories.Update(_mapper.Map<Category>(category));
            await _unitOfWork.SaveAsync();
        }

        public async Task<CategoryDto> GetByIdAsync(int id)
        {
            Category category = await _unitOfWork.Categories.GetByIdAsync(id);
            if (category == null)
            {
                throw new KeyNotFoundException("There is no such category");
            }
            return _mapper.Map<CategoryDto>(category);
        }

        public IEnumerable<CategoryDto> GetAll()
        {
            return _mapper.Map<IEnumerable<CategoryDto>>(_unitOfWork.Categories.GetAll().Where(x => !x.IsDeleted));
        }

        public IEnumerable<CategoryDto> GetAllWithDeleted()
        {
            return _mapper.Map<IEnumerable<CategoryDto>>(_unitOfWork.Categories.GetAll());
        }
    }
}
