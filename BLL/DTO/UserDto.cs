﻿using System;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class UserDto
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public List<string> UserRoles { get; set; }
        public List<string> AllRoles { get; set; }

        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Bio { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public DateTime? BirthDate { get; set; }
        public string EnglishLevel { get; set; }

        public List<string> UserTags { get; set; }
        public List<string> AllTags { get; set; }
        public List<string> EditedTags { get; set; }

        public UserDto()
        {
            UserRoles = new List<string>();
            AllRoles = new List<string>();
            UserTags = new List<string>();
            AllTags = new List<string>();
            EditedTags = new List<string>();
        }
    }
}
