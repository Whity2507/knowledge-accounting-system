﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    public class CommentDto
    {
        public int Id { get; set; }

        [Required]
        [MinLength(10, ErrorMessage = "Minimum length is 10 characters.")]
        [MaxLength(2000, ErrorMessage = "Maximum length is 2000 characters.")]
        public string Text { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime PostDate { get; set; }

        public int ArticleId { get; set; }
        public string ArticleName { get; set; }

        public string UserId { get; set; }
        public string NickName { get; set; }
    }
}
