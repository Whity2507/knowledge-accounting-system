﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    public class CategoryDto
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "Maximum length is 200 characters.")]
        public string Name { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "Minimum length is 5 characters.")]
        [MaxLength(200, ErrorMessage = "Maximum length is 200 characters.")]
        public string Description { get; set; }

        public bool IsDeleted { get; set; }
    }
}
