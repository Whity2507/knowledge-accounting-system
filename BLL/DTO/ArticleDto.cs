﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BLL.DTO
{
    public class ArticleDto
    {
        public int Id { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Minimum length is 2 characters.")]
        [MaxLength(200, ErrorMessage = "Maximum length must be 200 characters.")]
        public string Name { get; set; }

        [AllowHtml]
        [Required]
        [MinLength(200, ErrorMessage = "Minimum length is 2 characters.")]
        [MaxLength(15000, ErrorMessage = "Maximum length is 15000 characters.")]
        public string Text { get; set; }

        public DateTime PostDate { get; set; }
        public DateTime LastChangeDate { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public List<CategoryDto> AllCategories { get; set; }

        [Display(Name = "Tags")]
        public List<ArticleTagDto> ArticleTags { get; set; }
        public List<ArticleTagDto> AllTags { get; set; }

        [Required]
        [Display(Name = "Tags")]
        public List<int> EditedTagIds { get; set; }

        public ICollection<CommentDto> Comments { get; set; }

        public ArticleDto()
        {
            ArticleTags = new List<ArticleTagDto>();
            AllTags = new List<ArticleTagDto>();
            AllCategories = new List<CategoryDto>();
        }
    }
}