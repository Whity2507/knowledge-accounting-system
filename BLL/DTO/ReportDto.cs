﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    public class ReportDto
    {
        public int Id { get; set; }

        [Required]
        [MinLength(50, ErrorMessage = "Minimum length is 50 characters.")]
        [MaxLength(2000, ErrorMessage = "Maximum length is 2000 characters.")]
        public string Text { get; set; }

        public string UserEmail { get; set; }
    }
}
