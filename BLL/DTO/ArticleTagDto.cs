﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    public class ArticleTagDto
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(10, ErrorMessage = "Maximum length is 10 characters.")]
        public string Name { get; set; }
    }
}
