﻿using BLL.DTO;
using BLL.Interfaces;
using Moq;
using NUnit.Framework;
using PL.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Tests.PL.Tests
{
    public class ArticleTagControllerTests
    {
        private IEnumerable<ArticleTagDto> GetTestArticleTagDtos()
        {
            return new List<ArticleTagDto>()
            {
                new ArticleTagDto {Id = 1, Name = "C#" },
                new ArticleTagDto {Id = 2, Name = "Python" },
                new ArticleTagDto {Id = 3, Name = "Java" }
            }.AsEnumerable();
        }

        [Test]
        public void ArticleTagsController_List_ReturnsCorrectModel()
        {
            // Arrange
            var expected = GetTestArticleTagDtos();
            var mockService = new Mock<IArticleTagService>();
            mockService.Setup(x => x.GetAll()).Returns(GetTestArticleTagDtos());
            ArticleTagsController articleTagsController = new ArticleTagsController(mockService.Object);

            // Act
            var result = articleTagsController.List() as ViewResult;
            var actual = result.ViewData.Model as IEnumerable<ArticleTagDto>;

            // Assert
            Assert.That(actual, Is.EqualTo(expected).Using(new ArticleTagDtoEqualityComparer()));
        }

        [Test]
        public async Task ArticleTagsController_Add_AddsTag()
        {
            // Arrange
            var model = new ArticleTagDto { Name = "Assembler" };
            var expected = typeof(RedirectToRouteResult);
            var mockService = new Mock<IArticleTagService>();
            mockService.Setup(x => x.AddAsync(It.IsAny<ArticleTagDto>()));
            ArticleTagsController articleTagsController = new ArticleTagsController(mockService.Object);

            // Act
            var result = await articleTagsController.Add(model);

            // Assert
            Assert.AreEqual(expected, result.GetType());
            mockService.Verify(x => x.AddAsync(model), Times.Once);
        }
    }
}
