﻿using DAL.EF;
using DAL.Entities;
using DAL.Repositories;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.DAL.Tests
{
    public class ArticleTagRepositoryTests
    {
        [Test]
        public async Task ArticleTagRepository_Add_AddsValueToDatabase()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetTestDbConnection()))
            {
                var expectedTags = 8;
                var articleTagRepository = new ArticleTagRepository(context);
                var articleTag = new ArticleTag() { Name = "Assembler" };

                articleTagRepository.Add(articleTag);
                await context.SaveChangesAsync();

                Assert.AreEqual(expectedTags, context.ArticleTags.Count());
            }
        }

        [Test]
        public async Task ArticleTagRepository_Update_UpdatesEntity()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetTestDbConnection()))
            {
                var articleTagRepository = new ArticleTagRepository(context);

                var articleTag = new ArticleTag() { Id = 1, Name = "Assembler" };

                articleTagRepository.Update(articleTag);
                await context.SaveChangesAsync();

                articleTag = await context.ArticleTags.FindAsync(1);

                Assert.AreEqual(1, articleTag.Id);
                Assert.AreEqual("Assembler", articleTag.Name);
            }
        }

        [Test]
        public async Task ArticleTagRepository_Delete_DeletesEntity()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetTestDbConnection()))
            {
                var articleTagRepository = new ArticleTagRepository(context);

                await articleTagRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                var articleTag = await context.ArticleTags.FindAsync(1);

                Assert.IsNull(articleTag);
            }
        }

        [Test]
        public void ArticleTagRepository_GetAll_ReturnsAllValues()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetTestDbConnection()))
            {
                var expectedTags = 7;
                var articleTagRepository = new ArticleTagRepository(context);

                var articleTags = articleTagRepository.GetAll();

                Assert.AreEqual(expectedTags, articleTags.Count());
            }
        }

        [Test]
        public void ArticleTagRepository_GetAllWithDetails_ReturnsWithIncludedEntities()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetTestDbConnection()))
            {
                var expectedArticlesInTag = 6;
                var articleTagRepository = new ArticleTagRepository(context);
                var articleTagsWithIncludes = articleTagRepository.GetAllWithDetails();

                var actual = articleTagsWithIncludes.FirstOrDefault().Articles.Count;

                Assert.AreEqual(expectedArticlesInTag, actual);
            }
        }

        [Test]
        public async Task ArticleTagRepository_GetByIdAsync_ReturnsCorrectValue()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetTestDbConnection()))
            {
                var articleTagRepository = new ArticleTagRepository(context);

                var articleTag = await articleTagRepository.GetByIdAsync(1);

                Assert.AreEqual(1, articleTag.Id);
                Assert.AreEqual("C#", articleTag.Name);
            }
        }

        [Test]
        public async Task ArticleTagRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using (var context = new ApplicationContext(UnitTestHelper.GetTestDbConnection()))
            {
                var expectedArticlesInTag = 6;
                var articleTagRepository = new ArticleTagRepository(context);
                var articleTagsWithIncludes = await articleTagRepository.GetByIdWithDetailsAsync(1);

                var actual = articleTagsWithIncludes.Articles.Count;

                Assert.AreEqual(expectedArticlesInTag, actual);
            }
        }
    }
}
