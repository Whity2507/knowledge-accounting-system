﻿using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using BLL.Services;
using DAL.Entities;
using DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.BLL.Tests
{
    public class ArticleTagServiceTests
    {
        private IQueryable<ArticleTag> GetTestArticleTagEntities()
        {
            return new List<ArticleTag>()
            {
                new ArticleTag {Id = 1, Name = "C#" },
                new ArticleTag {Id = 2, Name = "Python" },
                new ArticleTag {Id = 3, Name = "Java" }
            }.AsQueryable();
        }

        private IEnumerable<ArticleTagDto> GetTestArticleTagDtos()
        {
            return new List<ArticleTagDto>()
            {
                new ArticleTagDto {Id = 1, Name = "C#" },
                new ArticleTagDto {Id = 2, Name = "Python" },
                new ArticleTagDto {Id = 3, Name = "Java" }
            }.AsEnumerable();
        }

        [Test]
        public async Task ArticleTagService_AddAsync_AddsEntity()
        {
            //Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleTags.Add(It.IsAny<ArticleTag>()));
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var articleTagDto = new ArticleTagDto { Id = 4, Name = "Golang" };

            // Act
            await articleTagService.AddAsync(articleTagDto);

            // Assert
            mockUnitOfWork.Verify(x => x.ArticleTags.Add(It.Is<ArticleTag>(y => y.Id == articleTagDto.Id && y.Name == articleTagDto.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task ArticleTagService_UpdateAsync_UpdatesEntity()
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleTags.Update(It.IsAny<ArticleTag>()));
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var articleTagDto = new ArticleTagDto { Id = 3, Name = "Golang" };

            // Act
            await articleTagService.UpdateAsync(articleTagDto);

            // Assert
            mockUnitOfWork.Verify(x => x.ArticleTags.Update(It.Is<ArticleTag>(y => y.Id == articleTagDto.Id && y.Name == articleTagDto.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(111)]
        public async Task ArticleTagService_DeleteByIdAsync_DeletesEntity(int id)
        {
            // Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleTags.DeleteByIdAsync(It.IsAny<int>()));
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            await articleTagService.DeleteByIdAsync(id);

            // Assert
            mockUnitOfWork.Verify(x => x.ArticleTags.DeleteByIdAsync(id), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task ArticleTagService_GetByIdAsync_ReturnsArticleTagDto()
        {
            // Arrange
            var expected = GetTestArticleTagDtos().First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.ArticleTags.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(GetTestArticleTagEntities().First());
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            var actual = await articleTagService.GetByIdAsync(1);

            // Assert
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Name, actual.Name);
        }

        [Test]
        public void ArticleTagService_GetAll_ReturnsArticleDtos()
        {
            // Arrange
            var expected = GetTestArticleTagDtos();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.ArticleTags.GetAll()).Returns(GetTestArticleTagEntities());
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            var actual = articleTagService.GetAll();

            // Assert
            Assert.That(actual, Is.EqualTo(expected).Using(new ArticleTagDtoEqualityComparer()));
        }

        [Test]
        public async Task ArticleTagService_GetTagsByArticleIdAsync_ReturnsArticleTagDtos()
        {
            // Arrange
            var article = new Article { ArticleTags = new List<ArticleTag>
            { 
                new ArticleTag {Id = 1, Name = "C#" },
                new ArticleTag {Id = 2, Name = "Python" },
                new ArticleTag {Id = 3, Name = "Java" }
            }
            };
            var expected = GetTestArticleTagDtos().ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.Articles.GetByIdWithDetailsAsync(It.IsAny<int>())).ReturnsAsync(article);
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            // Act
            var actual = await articleTagService.GetTagsByArticleIdAsync(It.IsAny<int>());

            // Assert
            Assert.That(actual, Is.EqualTo(expected).Using(new ArticleTagDtoEqualityComparer()));
        }

        [Test]
        public void ArticleTagService_AddAsync_ThrowsExceptionWithEmptyName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var articleTagDto = new ArticleTagDto { Name = "" };

            Assert.ThrowsAsync<DtoFieldException>(() => articleTagService.AddAsync(articleTagDto));
        }

        [Test]
        public void ArticleTagService_UpdateAsync_ThrowsExceptionWithEmptyName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            IArticleTagService articleTagService = new ArticleTagService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var articleTagDto = new ArticleTagDto { Name = "" };

            Assert.ThrowsAsync<DtoFieldException>(() => articleTagService.UpdateAsync(articleTagDto));
        }
    }
}
