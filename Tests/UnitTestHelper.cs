﻿using AutoMapper;
using BLL.Infrastructure;
using Effort;
using System.Data.Common;

namespace Tests
{
    public static class UnitTestHelper
    {
        public static DbConnection GetTestDbConnection()
        {
            var connection = DbConnectionFactory.CreateTransient();

            return connection;
        }

        public static Mapper CreateMapperProfile()
        {
            var automapperProfile = new AutomapperDtoProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(automapperProfile));

            return new Mapper(configuration);
        }
    }
}