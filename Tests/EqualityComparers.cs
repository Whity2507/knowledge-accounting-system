﻿using BLL.DTO;
using System.Collections.Generic;

namespace Tests
{
    class ArticleTagDtoEqualityComparer : IEqualityComparer<ArticleTagDto>
    {
        public bool Equals(ArticleTagDto x, ArticleTagDto y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;

            return x.Id == y.Id
                && x.Name == y.Name;
        }

        public int GetHashCode(ArticleTagDto obj)
        {
            return obj.GetHashCode();
        }
    }
}
