﻿using AutoMapper;
using BLL.DTO;
using PL.ViewModels;

namespace PL.Infrastructure
{
    public class AutomapperViewModelProfile : Profile
    {
        public AutomapperViewModelProfile()
        {
            CreateMap<SignUpViewModel, UserDto>()
                .ReverseMap();

            CreateMap<SignInViewModel, UserDto>()
                .ReverseMap();

            CreateMap<PublicProfileViewModel, UserDto>()
                .ForMember(y => y.EnglishLevel, x => x.MapFrom(publicProfile => publicProfile.EnglishLevel.ToString()))
                .ReverseMap();

            CreateMap<EditRoleViewModel, UserDto>()
                .ReverseMap();
        }
    }
}