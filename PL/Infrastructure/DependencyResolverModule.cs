﻿using AutoMapper;
using BLL.Infrastructure;
using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;

namespace PL.Infrastructure
{
    public class DependencyResolverModule : NinjectModule
    {
        public override void Load()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutomapperDtoProfile>();
                cfg.AddProfile<AutomapperViewModelProfile>();
            });
            Bind<IMapper>().ToConstructor(c => new Mapper(mapperConfiguration)).InSingletonScope();

            Bind<IArticleService>().To<ArticleService>();
            Bind<IUserService>().To<UserService>();
            Bind<IFilterService>().To<FilterService>();
            Bind<ICategoryService>().To<CategoryService>();
            Bind<IArticleTagService>().To<ArticleTagService>();
            Bind<ITechnologyTagService>().To<TechnologyTagService>();
            Bind<IReportService>().To<ReportService>();
            Bind<ICommentService>().To<CommentService>();
        }
    }
}