﻿using PL.Enums;

namespace PL.Models
{
    public class EnglishLevelModel
    {
        public EnglishLevel EnglishLevel { get; set; }
        public bool IsSelected { get; set; }
    }
}