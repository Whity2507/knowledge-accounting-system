﻿using System.ComponentModel.DataAnnotations;

namespace PL.Enums
{

    public enum EnglishLevel
    {
        [Display(Name = "Not selected")]
        Default,

        [Display(Name = "A1")]
        A1,

        [Display(Name = "A2")]
        A2,

        [Display(Name = "B1")]
        B1,

        [Display(Name = "B2")]
        B2,

        [Display(Name = "C1")]
        C1,

        [Display(Name = "C2")]
        C2
    }
}