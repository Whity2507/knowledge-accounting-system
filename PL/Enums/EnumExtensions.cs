﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace PL.Enums
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns value of Name property of Enum value
        /// </summary>
        /// <param name="enumValue">Enum value</param>
        /// <returns>Name, set by the attribute, if value is null returns "Not selected" string</returns>
        public static string GetEnumDisplayName(this Enum enumValue)
        {
            if(enumValue == null)
            {
                return "Not selected";
            }
            else
            {
                return enumValue.GetType()
                .GetMember(enumValue.ToString())
                .First()
                .GetCustomAttribute<DisplayAttribute>()
                .Name;
            }
        }
    }
}