﻿using System.ComponentModel.DataAnnotations;

namespace PL.ViewModels
{
    public class SignUpViewModel
    {
        [Required]
        [MinLength(2, ErrorMessage = "Minimum length is 2 characters.")]
        [MaxLength(50, ErrorMessage = "Maximum length is 50 characters.")]
        [Display(Name = "Nickname")]
        public string NickName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(6, ErrorMessage = "Minimum length is 6 characters.")]
        [MaxLength(100, ErrorMessage = "Maximum length is 100 characters.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}