﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PL.ViewModels
{
    public class EditRoleViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public IList<string> UserRoles { get; set; }
        public List<string> AllRoles { get; set; }

        [Required]
        public List<string> EditedRoles { get; set; }

        [Display(Name = "Nickname")]
        public string NickName { get; set; }
    }
}