﻿using System.Collections.Generic;

namespace PL.ViewModels
{
    public class UserFilterViewModel
    {
        public IEnumerable<PublicProfileViewModel> UserPublicProfiles { get; set; }
        public PublicProfileFiltersViewModel Filters { get; set; }
    }
}