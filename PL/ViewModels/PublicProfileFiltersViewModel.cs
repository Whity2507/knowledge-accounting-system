﻿using PL.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PL.ViewModels
{
    public class PublicProfileFiltersViewModel
    {
        [Display(Name = "English level")]
        public List<EnglishLevelModel> EnglishLevels { get; set; }

        [Display(Name = "Technologies")]
        public List<string> AllTags { get; set; }
        public List<string> SelectedTags { get; set; }
    }
}