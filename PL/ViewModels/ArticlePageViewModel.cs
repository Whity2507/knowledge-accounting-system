﻿using BLL.DTO;

namespace PL.ViewModels
{
    public class ArticlePageViewModel
    {
        public ArticleDto Article { get; set; }
        public CommentDto Comment { get; set; }
    }
}