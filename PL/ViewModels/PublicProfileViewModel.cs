﻿using PL.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PL.ViewModels
{
    public class PublicProfileViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Minimum length is 2 characters.")]
        [MaxLength(50, ErrorMessage = "Maximum length is 50 characters.")]
        [Display(Name = "Nickname")]
        public string NickName { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        public string Bio { get; set; }

        public string Company { get; set; }

        public string Location { get; set; }

        [Display(Name = "Date of birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "English level")]
        public EnglishLevel? EnglishLevel { get; set; }

        [Display(Name = "Technologies")]
        public IList<string> UserTags { get; set; }
        public List<string> AllTags { get; set; }
        public List<string> EditedTags { get; set; }
    }
}