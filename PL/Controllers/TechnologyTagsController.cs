﻿using BLL.DTO;
using BLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class TechnologyTagsController : Controller
    {
        private readonly ITechnologyTagService _technologyTagService;

        /// <summary>
        /// Injects dependencies of ITechnologyTagService implementation
        /// </summary>
        /// <param name="technologyTagService">Reference to the ITechnologyTagService interface</param>
        public TechnologyTagsController(ITechnologyTagService technologyTagService)
        {
            _technologyTagService = technologyTagService;
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="IEnumerable{}" /> of <see cref="TechnologyTagDto"/> model.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        public ActionResult List()
        {
            IEnumerable<TechnologyTagDto> technologyTags = _technologyTagService.GetAll();
            return View(technologyTags);
        }

        /// <summary>
        /// Processes the GET request and returns view.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Processes the POST request to add a new tag.
        /// </summary>
        /// <param name="model"><see cref="TechnologyTagDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(TechnologyTagDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await _technologyTagService.AddAsync(model);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="TechnologyTagDto"/> model.
        /// </summary>
        /// <param name="id">Tag id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Update(int id)
        {
            TechnologyTagDto model = await _technologyTagService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to update tag.
        /// </summary>
        /// <param name="model"><see cref="TechnologyTagDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(TechnologyTagDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await _technologyTagService.UpdateAsync(model);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Processes the GET request to delete tag confirmation.
        /// </summary>
        /// <param name="id">Tag id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            TechnologyTagDto model = await _technologyTagService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to delete tag.
        /// </summary>
        /// <param name="model"><see cref="TechnologyTagDto"/> model</param>
        /// <returns><see cref="TechnologyTagDto"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            await _technologyTagService.DeleteByIdAsync(id);

            return RedirectToAction("List");
        }
    }
}