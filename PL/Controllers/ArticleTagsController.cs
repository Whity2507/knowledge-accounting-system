﻿using BLL.DTO;
using BLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ArticleTagsController : Controller
    {
        private readonly IArticleTagService _articleTagService;

        /// <summary>
        /// Injects dependencies of IArticleTagService implementation
        /// </summary>
        /// <param name="articleTagService">Reference to the IArticleTagService interface</param>
        public ArticleTagsController(IArticleTagService articleTagService)
        {
            _articleTagService = articleTagService;
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="IEnumerable{}" /> of <see cref="ArticleTagDto"/> model.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        public ActionResult List()
        {
            IEnumerable<ArticleTagDto> articleTags = _articleTagService.GetAll();
            return View(articleTags);
        }

        /// <summary>
        /// Processes the GET request and returns view.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Processes the POST request to add a new tag.
        /// </summary>
        /// <param name="model"><see cref="ArticleTagDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(ArticleTagDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await _articleTagService.AddAsync(model);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="ArticleTagDto"/> model.
        /// </summary>
        /// <param name="id">Tag id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Update(int id)
        {
            ArticleTagDto model = await _articleTagService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to update tag.
        /// </summary>
        /// <param name="model"><see cref="ArticleTagDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(ArticleTagDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await _articleTagService.UpdateAsync(model);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Processes the GET request to delete tag confirmation.
        /// </summary>
        /// <param name="id">Tag id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            ArticleTagDto model = await _articleTagService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to delete tag.
        /// </summary>
        /// <param name="model"><see cref="ArticleTagDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            await _articleTagService.DeleteByIdAsync(id);

            return RedirectToAction("List");
        }
    }
}