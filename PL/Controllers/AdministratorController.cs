﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using PL.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdministratorController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUserService and IMapper implementation
        /// </summary>
        /// <param name="userService">Reference to the IUserService interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public AdministratorController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="IEnumerable{}"/> of <see cref="EditRoleViewModel"/> model.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        public ActionResult Users()
        {
            IEnumerable<EditRoleViewModel> model = _mapper.Map<IEnumerable<EditRoleViewModel>>(_userService.GetAll());
            return View(model);
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="EditRoleViewModel"/> model.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> EditRoles(string id)
        {
            EditRoleViewModel model = _mapper.Map<EditRoleViewModel>(await _userService.GetUserWithRolesByIdAsync(id));
            return View(model);
        }

        /// <summary>
        /// Processes the POST request to update user roles from <see cref="EditedRoles"/> field and returns view if it empty or redirects to EditRoles GET request.
        /// </summary>
        /// <param name="model"><see cref="EditRoleViewModel"/> model.</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditRoles(EditRoleViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "There must be at least one role for user");
                return View(model);
            }
            await _userService.UpdateUserRolesAsync(model.Id, model.EditedRoles);

            return RedirectToAction("EditRoles", new { id = model.Id });
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="UserDto"/> model.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Lock(string id)
        {
            UserDto model = await _userService.GetUserEmailByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to lock user account
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="RedirectToRouteResult"/> to <see cref="Users"/> action.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Lock")]
        public async Task<ActionResult> LockConfirm(string id)
        {
            await _userService.LockUserByIdAsync(id);

            return RedirectToAction("Users");
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="UserDto"/> model.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Unlock(string id)
        {
            UserDto model = await _userService.GetUserEmailByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to unlock user account
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="RedirectToRouteResult"/> to <see cref="Users"/> action.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Unlock")]
        public async Task<ActionResult> UnlockConfirm(string id)
        {
            await _userService.UnlockUserByIdAsync(id);

            return RedirectToAction("Users");
        }
    }
}