﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using PL.ViewModels;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of IUserService and IMapper implementation
        /// </summary>
        /// <param name="userService">Reference to the IUserService interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public AccountController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Processes the GET request to return view if user is authenticated and if not redirects to Recent method of ArticlesController.
        /// </summary>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpGet]
        public ActionResult SignIn()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }

            return RedirectToAction("Recent", "Articles");
        }

        /// <summary>
        /// Processes the POST request to authenticate user.
        /// </summary>
        /// <param name="model"><see cref="SignInViewModel"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignIn(SignInViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            UserDto userDto = _mapper.Map<UserDto>(model);

            var authenticationManager = HttpContext.GetOwinContext().Authentication;

            ClaimsIdentity userIdentity;
            try
            {
                userIdentity = await _userService.AuthenticateAsync(userDto);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }

            authenticationManager.SignIn(new AuthenticationProperties() {  }, userIdentity);

            return RedirectToAction("Recent", "Articles");
        }

        /// <summary>
        /// Processes the GET request to return view if user is not authenticated, otherwise redirects to Recent method of ArticlesController.
        /// </summary>
        /// <param name="model"><see cref="SignInViewModel"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpGet]
        public ActionResult SignUp()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }

            return RedirectToAction("Recent", "Articles");
        }

        /// <summary>
        /// Processes the POST request to add new user.
        /// </summary>
        /// <param name="model"><see cref="SignUpViewModel"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SignUp(SignUpViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            
            UserDto userDto = _mapper.Map<UserDto>(model);

            try
            {
                IdentityResult result = await _userService.AddAsync(userDto);
                if (result.Succeeded)
                {
                    return RedirectToAction("Recent", "Articles");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to revoke any claims identity associated with user.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult SignOut()
        {
            if (User.Identity.IsAuthenticated)
            { 
                var authenticationManager = HttpContext.GetOwinContext().Authentication;
                authenticationManager.SignOut();
            }

            return RedirectToAction("Recent", "Articles");
        }
    }
}