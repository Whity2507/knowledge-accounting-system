﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using PL.Enums;
using PL.Models;
using PL.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    [Authorize]
    public class PublicProfilesController : Controller
    {
        private readonly IUserService _userService;
        private readonly IFilterService _filterService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Injects dependencies of service implementations
        /// </summary>
        /// <param name="userService">Reference to the IUserService interface</param>
        /// <param name="filterService">Reference to the IFilterService interface</param>
        /// <param name="mapper">Reference to the IMapper interface</param>
        public PublicProfilesController(IUserService userService, IFilterService filterService, IMapper mapper)
        {
            _userService = userService;
            _filterService = filterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Processes the GET request and returns public profile of user.
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns><see cref="ViewResult"/> view with <see cref="PublicProfileViewModel"/> model.</returns>
        [HttpGet]
        public new async Task<ActionResult> Profile(string id)
        {
            var publicProfileViewModel = _mapper.Map<PublicProfileViewModel>(await _userService.GetUserProfileByIdAsync(id));
            return View(publicProfileViewModel);
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="PublicProfileViewModel"/> model.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Update()
        {
            string userId = User.Identity.GetUserId();
            var publicProfileViewModel = _mapper.Map<PublicProfileViewModel>(await _userService.GetUserProfileByIdAsync(userId));
            return View(publicProfileViewModel);
        }

        /// <summary>
        /// Processes the POST request to update public profile.
        /// </summary>
        /// <param name="model"><see cref="PublicProfileViewModel"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(PublicProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var userProfile = _mapper.Map<PublicProfileViewModel>(await _userService.GetUserProfileByIdAsync(userId));
                return View(userProfile);
            }

            var editedUserProfile = _mapper.Map<UserDto>(model);
            editedUserProfile.Id = User.Identity.GetUserId();
            await _userService.UpdateProfileAsync(editedUserProfile);

            return RedirectToAction("Update");
        }

        /// <summary>
        /// Processes the GET request to return view with user filter.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view with <see cref="UserFilterViewModel"/> model.</returns>
        [HttpGet]
        [Authorize(Roles = "Manager")]
        public ActionResult Filter()
        {
            UserFilterViewModel model = new UserFilterViewModel();
            model.Filters = new PublicProfileFiltersViewModel();
            model.Filters.AllTags = _filterService.GetAllTechnologyTagNames();
            model.Filters.EnglishLevels = new List<EnglishLevelModel>();
            foreach (EnglishLevel value in Enum.GetValues(typeof(EnglishLevel)))
            {
                model.Filters.EnglishLevels.Add(new EnglishLevelModel() { EnglishLevel = value, IsSelected = false });
            }

            model.UserPublicProfiles = _mapper.Map<IEnumerable<PublicProfileViewModel>>(_userService.GetUserProfilesWithEmail());

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to return the list of users according to the filter.
        /// </summary>
        /// <param name="model"></param>
        /// <returns><see cref="ViewResult"/> view with <see cref="UserFilterViewModel"/> model.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Manager")]
        public ActionResult Filter(UserFilterViewModel model)
        {
            List<string> technologyTags = model.Filters.SelectedTags;
            List<string> englishLevels = model.Filters.EnglishLevels
                .Where(x => x.IsSelected)
                .Select(x => x.EnglishLevel.ToString())
                .ToList();

            model.UserPublicProfiles = _mapper.Map<IEnumerable<PublicProfileViewModel>>(_filterService.GetUsersByFilter(englishLevels, technologyTags));
            model.Filters.AllTags = _filterService.GetAllTechnologyTagNames();

            return View(model);
        }
    }
}