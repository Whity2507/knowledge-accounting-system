﻿using BLL.DTO;
using BLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class ReportsController : Controller
    {
        private readonly IReportService _reportService;

        /// <summary>
        /// Injects dependencies of IReportService implementation
        /// </summary>
        /// <param name="reportService">Reference to the IReportService interface</param>
        public ReportsController(IReportService reportService)
        {
            _reportService = reportService;
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="IEnumerable{}" /> of <see cref="ReportDto"/> model.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [Authorize(Roles = "Administrator")]
        public ActionResult List()
        {
            IEnumerable<ReportDto> reports = _reportService.GetAll();
            return View(reports);
        }

        /// <summary>
        /// Processes the GET request and returns view.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        [Authorize]
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Processes the POST request to add a new report.
        /// </summary>
        /// <param name="model"><see cref="ReportDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(ReportDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            model.UserEmail = User.Identity.Name;
            await _reportService.AddAsync(model);

            return RedirectToAction("Confirmed");
        }


        [HttpGet]
        public ActionResult Confirmed()
        {
            return View();
        }

        /// <summary>
        /// Processes the GET request to delete report confirmation.
        /// </summary>
        /// <param name="id">Tag id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Delete(int id)
        {
            ReportDto model = await _reportService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to delete report.
        /// </summary>
        /// <param name="model"><see cref="ReportDto"/> model</param>
        /// <returns><see cref="ReportDto"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            await _reportService.DeleteByIdAsync(id);

            return RedirectToAction("List");
        }
    }
}