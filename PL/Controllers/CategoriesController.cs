﻿using BLL.DTO;
using BLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;

        /// <summary>
        /// Injects dependencies of ICategoryService implementation
        /// </summary>
        /// <param name="categoryService">Reference to the ICategoryService interface</param>
        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="IEnumerable{}" /> of <see cref="CategoryDto"/> model.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        public ActionResult List()
        {
            IEnumerable<CategoryDto> categories = _categoryService.GetAllWithDeleted();
            return View(categories);
        }

        /// <summary>
        /// Processes the GET request and returns view.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Processes the POST request to add a new category.
        /// </summary>
        /// <param name="model"><see cref="CategoryDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(CategoryDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await _categoryService.AddAsync(model);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="CategoryDto"/> model.
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Update(int id)
        {
            CategoryDto model = await _categoryService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to update category.
        /// </summary>
        /// <param name="model"><see cref="CategoryDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(CategoryDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await _categoryService.UpdateAsync(model);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Processes the GET request to delete category.
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            CategoryDto categoryDto = await _categoryService.GetByIdAsync(id);

            return View(categoryDto);
        }

        /// <summary>
        /// Processes the POST request to delete category.
        /// </summary>
        /// <param name="model"><see cref="CategoryDto"/> model</param>
        /// <returns><see cref="CategoryDto"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            await _categoryService.DeleteByIdAsync(id);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Processes the GET request to restore category confirmation.
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Restore(int id)
        {
            CategoryDto categoryDto = await _categoryService.GetByIdAsync(id);

            return View(categoryDto);
        }

        /// <summary>
        /// Processes the POST request to restore category.
        /// </summary>
        /// <param name="model"><see cref="CategoryDto"/> model</param>
        /// <returns><see cref="CategoryDto"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Restore")]
        public async Task<ActionResult> RestoreConfirm(int id)
        {
            await _categoryService.RestoreByIdAsync(id);

            return RedirectToAction("List");
        }
    }
}