﻿using System.Web.Mvc;

namespace PL.Controllers
{
    public class ErrorsController : Controller
    {
        /// <summary>
        /// Sets Response.StatusCode to 404 and returns view.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        public ActionResult NotFound()
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.NotFound;
            return View();
        }

        /// <summary>
        /// Sets Response.StatusCode to 500 and returns view.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        public ActionResult InternalError()
        {
            Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
            return View();
        }
    }
}