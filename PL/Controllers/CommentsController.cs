﻿using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using PL.ViewModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly IArticleService _articleService;

        /// <summary>
        /// Injects dependencies of service implementations
        /// </summary>
        /// <param name="commentService">Reference to the ICommentService interface</param>
        /// <param name="articleService">Reference to the IArticleService interface</param>
        public CommentsController(ICommentService commentService, IArticleService articleService)
        {
            _commentService = commentService;
            _articleService = articleService;
        }

        /// <summary>
        /// Processes the POST request to add a new comment.
        /// </summary>
        /// <param name="model"><see cref="ArticlePageViewModel"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(ArticlePageViewModel model)
        {
            var validationResults = new List<ValidationResult>();
            var validationContext = new ValidationContext(model.Comment);
            if (!Validator.TryValidateObject(model.Comment, validationContext, validationResults, true))
            {
                model.Article = await _articleService.GetByIdAsync(model.Article.Id);
                return View("Article", model);
            }

            model.Comment.UserId = User.Identity.GetUserId();
            model.Comment.ArticleId = model.Article.Id;

            await _commentService.AddAsync(model.Comment);

            return RedirectToAction("Article", "Articles", new { id = model.Article.Id });
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="CommentDto"/> model.
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Update(int id)
        {
            CommentDto model = await _commentService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to update comment.
        /// </summary>
        /// <param name="model"><see cref="CommentDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(CommentDto model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            await _commentService.UpdateAsync(model);

            return RedirectToAction("Article", "Articles", new { id = model.ArticleId });
        }

        /// <summary>
        /// Processes the GET request to delete comment confirmation.
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <returns><see cref="ViewResult"/> view.</returns>
        [HttpGet]
        public async Task<ActionResult> Delete(int id)
        {
            CommentDto model = await _commentService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to delete comment.
        /// </summary>
        /// <param name="model"><see cref="CommentDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(CommentDto model)
        {
            await _commentService.DeleteByIdAsync(model.Id);

            return RedirectToAction("Article", "Articles", new { id = model.ArticleId });
        }
    }
}