﻿using BLL.DTO;
using BLL.Interfaces;
using PagedList;
using PL.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly IArticleTagService _articleTagService;
        private readonly ICategoryService _categoryService;
        private readonly IFilterService _filterService;

        /// <summary>
        /// Injects dependencies of service implementations
        /// </summary>
        /// <param name="articleService">Reference to the IArticleService interface</param>
        /// <param name="articleTagService">Reference to the IArticleTagService interface</param>
        /// <param name="categoryService">Reference to the ICategoryService interface</param>
        /// <param name="filterService">Reference to the IFilterService interface</param>
        public ArticlesController(IArticleService articleService, IArticleTagService articleTagService, ICategoryService categoryService, IFilterService filterService)
        {
            _articleService = articleService;
            _articleTagService = articleTagService;
            _categoryService = categoryService;
            _filterService = filterService;
        }

        /// <summary>
        /// Processes the GET request and returns all articles ordered by date descending. 
        /// </summary>
        /// <param name="page">Number of current page</param>
        /// <returns><see cref="ViewResult"/> view with paged list of <see cref="ArticleDto"/>.</returns>
        public ActionResult Recent(int? page)
        {
            IEnumerable<ArticleDto> model = _articleService.GetAll();
            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Processes the POST request and returns all categories.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view.</returns>
        public ActionResult Categories()
        {
            IEnumerable<CategoryDto> model = _categoryService.GetAll();
            return View(model);
        }

        /// <summary>
        /// Processes the GET request and returns all articles in certain category, ordered by date descending. 
        /// </summary>
        /// <param name="id">Category id</param>
        /// <param name="page">Number of current page</param>
        /// <returns><see cref="ViewResult"/> view with paged list of <see cref="ArticleDto"/>.</returns>
        public async Task<ActionResult> Category(int id, int? page)
        {
            IEnumerable<ArticleDto> model = await _filterService.GetArticlesWithDetailsByCategoryIdAsync(id);
            ViewBag.Category = await _categoryService.GetByIdAsync(id);
            int pageSize = 5;
            int pageNumber = page ?? 1;
            return View(model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Processes the GET request and returns all articles with certain tag, ordered by date descending. 
        /// </summary>
        /// <param name="id">Category id</param>
        /// <param name="page">Number of current page</param>
        /// <returns><see cref="ViewResult"/> view with paged list of <see cref="ArticleDto"/>.</returns>
        public async Task<ActionResult> Tag(int id, int? page)
        {
            int pageSize = 5;
            int pageNumber = page ?? 1;
            IEnumerable<ArticleDto> model = await _filterService.GetArticlesWithDetailsByTagIdAsync(id);
            ViewBag.Tag = await _articleTagService.GetByIdAsync(id);
            return View(model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Processes the GET request and returns view with full version of article.
        /// </summary>
        /// <param name="id">Category id</param>
        /// <returns><see cref="ViewResult"/> view with <see cref="ArticlePageViewModel"/> model.</returns>
        public async Task<ActionResult> Article(int id)
        {
            ArticlePageViewModel model = new ArticlePageViewModel();
            model.Article = await _articleService.GetByIdAsync(id);
            model.Comment = new CommentDto();
            return View(model);
        }

        /// <summary>
        /// Processes the GET request and returns view with empty <see cref="ArticleDto"/> model.
        /// </summary>
        /// <returns><see cref="ViewResult"/> view with <see cref="ArticleDto"/> model.</returns>
        [HttpGet]
        [Authorize(Roles = "Writer")]
        public ActionResult Add()
        {
            var model = new ArticleDto
            {
                AllTags = _articleTagService.GetAll().ToList(),
                AllCategories = _categoryService.GetAll().ToList()
            };
            return View(model);
        }

        /// <summary>
        /// Processes the POST request to add a new article.
        /// </summary>
        /// <param name="model"><see cref="ArticleDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Writer")]
        public async Task<ActionResult> Add(ArticleDto model)
        {
            if (!ModelState.IsValid)
            {
                model.AllTags = _articleTagService.GetAll().ToList();
                model.AllCategories = _categoryService.GetAll().ToList();
                return View(model);
            }

            await _articleService.AddAsync(model);

            return RedirectToAction("Recent");
        }

        /// <summary>
        /// Processes the GET request and returns view with <see cref="ArticleDto"/> model.
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns><see cref="ViewResult"/> view with <see cref="ArticleDto"/> model.</returns>
        [HttpGet]
        [Authorize(Roles = "Writer")]
        public async Task<ActionResult> Update(int id)
        {
            ArticleDto model = await _articleService.GetByIdAsync(id);
            model.AllTags = _articleTagService.GetAll().ToList();
            model.AllCategories = _categoryService.GetAll().ToList();

            return View(model);
        }

        /// <summary>
        /// Processes the POST request to update tag.
        /// </summary>
        /// <param name="model"><see cref="ArticleTagDto"/> model</param>
        /// <returns><see cref="ActionResult"/> that can be <see cref="RedirectToRouteResult"/> or <see cref="ViewResult"/>.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Writer")]
        public async Task<ActionResult> Update(ArticleDto model)
        {
            if (!ModelState.IsValid)
            {
                model.ArticleTags = await _articleTagService.GetTagsByArticleIdAsync(model.Id);
                model.AllTags = _articleTagService.GetAll().ToList();
                model.AllCategories = _categoryService.GetAll().ToList();
                return View(model);
            }

            await _articleService.UpdateAsync(model);

            return RedirectToAction("Article", new { id = model.Id });
        }

        /// <summary>
        /// Processes the GET request to delete article confirmation.
        /// </summary>
        /// <param name="id">Tag id</param>
        /// <returns><see cref="ViewResult"/> view with <see cref="ArticleDto"/> model.</returns>
        [HttpGet]
        [Authorize(Roles = "Writer")]
        public async Task<ActionResult> Delete(int id)
        {
            ArticleDto model = await _articleService.GetByIdAsync(id);

            return View(model);
        }

        /// <summary>
        /// Processes the GET request to delete article.
        /// </summary>
        /// <param name="id">Article id</param>
        /// <returns><see cref="RedirectToRouteResult"/> view.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Writer")]
        [ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            await _articleService.DeleteByIdAsync(id);

            return RedirectToAction("Recent");
        }

        /// <summary>
        /// Processes the GET request and returns view with random article. 
        /// </summary>
        /// <returns><see cref="ViewResult"/> view with <see cref="ArticleDto"/> model.</returns>
        public async Task<ActionResult> RandomArticle()
        {
            ArticlePageViewModel model = new ArticlePageViewModel();
            model.Comment = new CommentDto();
            model.Article = await _articleService.GetRandomArticleAsync();
            model.Comment = new CommentDto();
            return View("Article", model);
        }
    }
}