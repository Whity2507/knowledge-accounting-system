﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ArticleTagRepository : IRepository<ArticleTag>
    {
        private readonly ApplicationContext _applicationContext;

        public ArticleTagRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(ArticleTag entity)
        {
            _applicationContext.ArticleTags.Add(entity);
        }

        public void Update(ArticleTag entity)
        {
            var item = _applicationContext.ArticleTags.Attach(entity);
            if (item != null)
            {
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await _applicationContext.ArticleTags.FindAsync(id);
            if (item != null)
            {
                _applicationContext.ArticleTags.Remove(item);
            }
        }

        public IQueryable<ArticleTag> GetAll()
        {
            return _applicationContext.ArticleTags;
        }

        public IQueryable<ArticleTag> GetAllWithDetails()
        {
            return _applicationContext.ArticleTags.Include(x => x.Articles);
        }

        public async Task<ArticleTag> GetByIdAsync(int id)
        {
            return await _applicationContext.ArticleTags.FindAsync(id);
        }

        public async Task<ArticleTag> GetByIdWithDetailsAsync(int id)
        {
            return await _applicationContext.ArticleTags.Where(x => x.Id == id).Include(x => x.Articles).FirstOrDefaultAsync();
        }
    }
}
