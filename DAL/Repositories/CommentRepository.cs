﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CommentRepository : IRepository<Comment>
    {
        private readonly ApplicationContext _applicationContext;

        public CommentRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(Comment entity)
        {
            _applicationContext.Comments.Add(entity);
        }

        public void Update(Comment entity)
        {
            var item = _applicationContext.Comments.Attach(entity);
            if (item != null)
            {
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await _applicationContext.Comments.FindAsync(id);
            if (item != null)
            {
                _applicationContext.Comments.Remove(item);
            }
        }

        public IQueryable<Comment> GetAll()
        {
            return _applicationContext.Comments;
        }

        public IQueryable<Comment> GetAllWithDetails()
        {
            return _applicationContext.Comments.Include(x => x.Article).Include(x => x.UserProfile);
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _applicationContext.Comments.FindAsync(id);
        }

        public async Task<Comment> GetByIdWithDetailsAsync(int id)
        {
            return await _applicationContext.Comments.Include(x => x.Article).Include(x => x.UserProfile).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
