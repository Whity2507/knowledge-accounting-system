﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _applicationContext;
        private ArticleRepository _articleRepository;
        private CommentRepository _commentRepository;
        private ArticleTagRepository _articleTagRepository;
        private CategoryRepository _categoryRepository;
        private TechnologyTagRepository _technologyTagRepository;
        private ReportRepository _reportRepository;
        private UserProfileRepository _userProfileRepository;
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;

        public UnitOfWork(string connectionString)
        {
            _applicationContext = new ApplicationContext(connectionString);
        }

        public IRepository<Article> Articles
        {
            get
            {
                if (_articleRepository == null)
                {
                    _articleRepository = new ArticleRepository(_applicationContext);
                }
                return _articleRepository;
            }
        }

        public IRepository<Comment> Comments
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentRepository(_applicationContext);
                }
                return _commentRepository;
            }
        }

        public IRepository<ArticleTag> ArticleTags
        {
            get
            {
                if (_articleTagRepository == null)
                {
                    _articleTagRepository = new ArticleTagRepository(_applicationContext);
                }
                return _articleTagRepository;
            }
        }

        public IRepository<Category> Categories
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new CategoryRepository(_applicationContext);
                }
                return _categoryRepository;
            }
        }

        public IRepository<TechnologyTag> TechnologyTags
        {
            get
            {
                if (_technologyTagRepository == null)
                {
                    _technologyTagRepository = new TechnologyTagRepository(_applicationContext);
                }
                return _technologyTagRepository;
            }
        }

        public IRepository<Report> Reports
        {
            get
            {
                if (_reportRepository == null)
                {
                    _reportRepository = new ReportRepository(_applicationContext);
                }
                return _reportRepository;
            }
        }

        public IUserProfileRepository UserProfiles
        {
            get
            {
                if (_userProfileRepository == null)
                {
                    _userProfileRepository = new UserProfileRepository(_applicationContext);
                }
                return _userProfileRepository;
            }
        }

        public UserManager<ApplicationUser> UserManager
        {
            get 
            {
                if (_userManager == null)
                {
                    _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_applicationContext));
                }
                return _userManager; 
            }
        }

        public RoleManager<ApplicationRole> RoleManager
        {
            get 
            {
                if (_roleManager == null)
                {
                    _roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(_applicationContext));
                }
                return _roleManager; 
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _applicationContext.SaveChangesAsync();
        }
    }
}
