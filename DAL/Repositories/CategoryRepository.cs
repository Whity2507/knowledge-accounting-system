﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly ApplicationContext _applicationContext;

        public CategoryRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(Category entity)
        {
            _applicationContext.Categories.Add(entity);
        }

        public void Update(Category entity)
        {
            var item = _applicationContext.Categories.Attach(entity);
            if (item != null)
            {
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await _applicationContext.Categories.FindAsync(id);
            if (item != null)
            {
                item.IsDeleted = true;
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public IQueryable<Category> GetAll()
        {
            return _applicationContext.Categories;
        }

        public IQueryable<Category> GetAllWithDetails()
        {
            return _applicationContext.Categories.Include(x => x.Articles);
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            return await _applicationContext.Categories.FindAsync(id);
        }

        public async Task<Category> GetByIdWithDetailsAsync(int id)
        {
            return await _applicationContext.Categories.Include(x => x.Articles).FirstOrDefaultAsync();
        }
    }
}
