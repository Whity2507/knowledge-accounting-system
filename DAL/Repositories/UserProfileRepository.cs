﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly ApplicationContext _applicationContext;

        public UserProfileRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(UserProfile entity)
        {
            _applicationContext.UserProfiles.Add(entity);
        }

        public void Update(UserProfile entity)
        {
            var item = _applicationContext.UserProfiles.Attach(entity);
            if (item != null)
            {
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public async Task DeleteByIdAsync(string id)
        {
            var item = await _applicationContext.UserProfiles.FindAsync(id);
            if (item != null)
            {
                _applicationContext.UserProfiles.Remove(item);
            }
        }

        public IQueryable<UserProfile> GetAll()
        {
            return _applicationContext.UserProfiles;
        }

        public IQueryable<UserProfile> GetAllWithDetails()
        {
            return _applicationContext.UserProfiles.Include(x => x.TechnologyTags).Include(x => x.ApplicationUser);
        }

        public async Task<UserProfile> GetByIdAsync(string id)
        {
            return await _applicationContext.UserProfiles.FindAsync(id);
        }

        public async Task<UserProfile> GetByIdWithDetailsAsync(string id)
        {
            return await _applicationContext.UserProfiles.Include(x => x.TechnologyTags).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
