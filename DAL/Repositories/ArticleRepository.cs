﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ArticleRepository : IRepository<Article>
    {
        private readonly ApplicationContext _applicationContext;

        public ArticleRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(Article entity)
        {
            _applicationContext.Articles.Add(entity);
        }

        public void Update(Article entity)
        {
            var item = _applicationContext.Articles.Attach(entity);
            if (item != null)
            {
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await _applicationContext.Articles.FindAsync(id);
            if (item != null)
            {
                _applicationContext.Articles.Remove(item);
            }
        }

        public IQueryable<Article> GetAll()
        {
            return _applicationContext.Articles;
        }

        public IQueryable<Article> GetAllWithDetails()
        {
            return _applicationContext.Articles.Include(x => x.ArticleTags).Include(x => x.Comments);
        }

        public async Task<Article> GetByIdAsync(int id)
        {
            return await _applicationContext.Articles.FindAsync(id);
        }

        public async Task<Article> GetByIdWithDetailsAsync(int id)
        {
            return await _applicationContext.Articles.Where(x => x.Id == id)
                .Include(x => x.ArticleTags)
                .Include(x => x.Category)
                .Include(x => x.Comments.Select(y => y.UserProfile))
                .FirstOrDefaultAsync();
        }
    }
}
