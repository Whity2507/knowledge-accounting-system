﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TechnologyTagRepository : IRepository<TechnologyTag>
    {
        private readonly ApplicationContext _applicationContext;

        public TechnologyTagRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(TechnologyTag entity)
        {
            _applicationContext.TechnologyTags.Add(entity);
        }

        public void Update(TechnologyTag entity)
        {
            var item = _applicationContext.TechnologyTags.Attach(entity);
            if (item != null)
            {
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await _applicationContext.TechnologyTags.FindAsync(id);
            if (item != null)
            {
                _applicationContext.TechnologyTags.Remove(item);
            }
        }

        public IQueryable<TechnologyTag> GetAll()
        {
            return _applicationContext.TechnologyTags;
        }

        public IQueryable<TechnologyTag> GetAllWithDetails()
        {
            return _applicationContext.TechnologyTags.Include(x => x.UserProfiles);
        }

        public async Task<TechnologyTag> GetByIdAsync(int id)
        {
            return await _applicationContext.TechnologyTags.FindAsync(id);
        }

        public async Task<TechnologyTag> GetByIdWithDetailsAsync(int id)
        {
            return await _applicationContext.TechnologyTags.Include(x => x.UserProfiles).FirstOrDefaultAsync();
        }
    }
}
