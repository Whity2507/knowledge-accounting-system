﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ReportRepository : IRepository<Report>
    {
        private readonly ApplicationContext _applicationContext;

        public ReportRepository(ApplicationContext applicationContext)
        {
            _applicationContext = applicationContext;
        }

        public void Add(Report entity)
        {
            _applicationContext.Reports.Add(entity);
        }

        public void Update(Report entity)
        {
            var item = _applicationContext.Reports.Attach(entity);
            if (item != null)
            {
                _applicationContext.Entry(item).State = EntityState.Modified;
            }
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await _applicationContext.Reports.FindAsync(id);
            if (item != null)
            {
                _applicationContext.Reports.Remove(item);
            }
        }

        public IQueryable<Report> GetAll()
        {
            return _applicationContext.Reports;
        }

        /// <summary>
        /// Not implemented because <see cref="Report"/> entity doesn't have related entities.
        /// </summary>
        public IQueryable<Report> GetAllWithDetails()
        {
            throw new NotImplementedException();
        }

        public async Task<Report> GetByIdAsync(int id)
        {
            return await _applicationContext.Reports.FindAsync(id);
        }

        /// <summary>
        /// Not implemented because <see cref="Report"/> entity doesn't have related entities.
        /// </summary>
        public Task<Report> GetByIdWithDetailsAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
