﻿using System.Linq;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    /// <summary>
    /// Decsribes CRUD methods to work with database.
    /// </summary>
    /// <typeparam name="T">Entity</typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Adds entity to database.
        /// </summary>
        void Add(T entity);

        /// <summary>
        /// Update entity in database.
        /// </summary>
        void Update(T entity);

        /// <summary>
        /// Delete certain entity from database.
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task DeleteByIdAsync(int id);

        /// <summary>
        /// Reads certain entity from database.
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="T"/>.</returns>
        Task<T> GetByIdAsync(int id);

        /// <summary>
        /// Reads certain entity from database table with related objects.
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>A task that represents the asynchronous operation that can return value of <see cref="T"/>.</returns>
        Task<T> GetByIdWithDetailsAsync(int id);

        /// <summary>
        /// Reads all entities from database table without related objects.
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of objects from database.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Reads all entities from database table with related objects.
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of objects from database.</returns>
        IQueryable<T> GetAllWithDetails();
    }
}
