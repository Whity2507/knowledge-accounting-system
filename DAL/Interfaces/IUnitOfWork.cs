﻿using DAL.Entities;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Article> Articles { get; }
        IRepository<Comment> Comments { get; }
        IRepository<ArticleTag> ArticleTags { get; }
        IRepository<Category> Categories { get; }
        IRepository<TechnologyTag> TechnologyTags { get; }
        IRepository<Report> Reports { get; }
        IUserProfileRepository UserProfiles { get; }
        UserManager<ApplicationUser> UserManager { get; }
        RoleManager<ApplicationRole> RoleManager { get; }

        Task<int> SaveAsync();
    }
}
