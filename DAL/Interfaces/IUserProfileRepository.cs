﻿using DAL.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUserProfileRepository
    {
        void Add(UserProfile entity);
        void Update(UserProfile entity);
        Task DeleteByIdAsync(string id);

        Task<UserProfile> GetByIdAsync(string id);
        Task<UserProfile> GetByIdWithDetailsAsync(string id);

        IQueryable<UserProfile> GetAll();
        IQueryable<UserProfile> GetAllWithDetails();
    }
}
