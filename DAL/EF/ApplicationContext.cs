﻿using DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Common;
using System.Data.Entity;

namespace DAL.EF
{
    public class ApplicationContext : IdentityDbContext
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleTag> ArticleTags { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<TechnologyTag> TechnologyTags { get; set; }
        public DbSet<Report> Reports { get; set; }

        static ApplicationContext()
        {
            Database.SetInitializer<ApplicationContext>(new DataBaseInitializer());
        }

        public ApplicationContext() : base() { }

        public ApplicationContext(string connectionString) : base(connectionString) { }

        public ApplicationContext(DbConnection connection) : base(connection, false) { }
    }
}
