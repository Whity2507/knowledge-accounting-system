﻿namespace DAL.Entities
{
    public class Report
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string UserEmail { get; set; }
    }
}
