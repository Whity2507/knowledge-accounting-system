﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class Article
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; }
        public DateTime LastChangeDate { get; set; }

        public int? CategoryId { get; set; }
        public Category Category { get; set; }

        public ICollection<ArticleTag> ArticleTags { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
