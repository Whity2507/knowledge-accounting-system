﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class TechnologyTag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<UserProfile> UserProfiles { get; set; }
    }
}
