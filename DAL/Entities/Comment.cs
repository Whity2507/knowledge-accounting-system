﻿using System;

namespace DAL.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime PostDate { get; set; }

        public int ArticleId { get; set; }
        public Article Article { get; set; }

        public string UserProfileId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}
